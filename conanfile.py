from conans import ConanFile, CMake, tools
import shutil

class MicrotarConan(ConanFile):
    name        = "microtar"
    version     = "0.1.0"
    license     = "MIT"
    author      = "toge.mail@gmail.com"
    url         = "https://bitbucket.org/toge/conan-microtar/"
    homepage    = "https://github.com/rxi/microtar"
    description = "A lightweight tar library written in ANSI C"
    topics      = ("tar", "ANSI C")
    settings    = "os", "compiler", "build_type", "arch"
    generators  = "cmake"
    exports     = "CMakeLists.txt"

    def source(self):
        tools.get("https://github.com/rxi/microtar/archive/v{}.zip".format(self.version))
        shutil.move("microtar-{}".format(self.version), "microtar")
        shutil.copy("CMakeLists.txt", "microtar")

    def build(self):
        cmake = CMake(self)
        cmake.configure(source_folder="microtar")
        cmake.build()

    def package(self):
        self.copy("*.h",     dst="include", src="microtar/src")
        self.copy("*.lib",   dst="lib",     keep_path=False)
        self.copy("*.dll",   dst="bin",     keep_path=False)
        self.copy("*.so",    dst="lib",     keep_path=False)
        self.copy("*.dylib", dst="lib",     keep_path=False)
        self.copy("*.a",     dst="lib",     keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["microtar"]
